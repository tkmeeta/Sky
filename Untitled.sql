-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: webadmin
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `metaconfig` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keys_UNIQUE` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES (1,'dict','{\n    \"entityName\": \"org.supercall.dao.SysDictMapper\",\n    \"gridColumnConfigs\": [\n        {\n            \"colunmName\": \"id\"\n        },\n        {\n            \"colunmName\": \"字段分组\"\n        },\n        {\n            \"colunmName\": \"字典键\"\n        },\n        {\n            \"colunmName\": \"字典值\"\n        }\n    ],\n    \"tableFilterConfigs\": [\n        {\n            \"columnName\": \"dictgroup\",\n            \"displayName\": \"字典分组\",\n            \"columnType\": \"input\",\n            \"dataSourcePluginName\": \" \",\n            \"dataSourcePlginFunction\": \" \"\n        },\n        {\n            \"columnName\": \"dictkey\",\n            \"displayName\": \"字段键\",\n            \"columnType\": \"input\",\n            \"dataSourcePluginName\": \" \",\n            \"dataSourcePlginFunction\": \" \"\n        }\n    ],\n    \"formConfigs\": [\n        {\n            \"displayName\": \"字典分组\",\n            \"key\": \"dictgroup\",\n            \"inputType\": \"input\",\n            \"pluginName\": \" \",\n            \"pluginFunction\": \" \",\n            \"isRequire\": true,\n            \"validateType\": \" \"\n        },\n        {\n            \"displayName\": \"字典键\",\n            \"key\": \"dictkey\",\n            \"inputType\": \"input\",\n            \"pluginName\": \" \",\n            \"pluginFunction\": \" \",\n            \"isRequire\": true,\n            \"validateType\": \" \"\n        },\n        {\n            \"displayName\": \"字典值\",\n            \"key\": \"dictvalues\",\n            \"inputType\": \"input\",\n            \"pluginName\": \" \",\n            \"pluginFunction\": \" \",\n            \"isRequire\": true,\n            \"validateType\": \" \"\n        }\n    ]\n}'),(2,'menu','{\n    \"entityName\": \"org.supercall.dao.SysMenuMapper\",\n    \"gridColumnConfigs\": [\n        {\n            \"colunmName\": \"id\"\n        },\n        {\n            \"colunmName\": \"上级菜单\"\n        },\n        {\n            \"colunmName\": \"菜单名称\"\n        },\n        {\n            \"colunmName\": \"菜单排序\"\n        },\n        {\n            \"colunmName\": \"菜单图标\"\n        },\n        {\n            \"colunmName\": \"菜单地址\"\n        },\n        {\n            \"colunmName\": \"是否隐藏\"\n        }\n    ],\n    \"tableFilterConfigs\": [\n        {\n            \"columnName\": \"name\",\n            \"displayName\": \"菜单名称\",\n            \"columnType\": \"input\"\n        },\n        {\n            \"columnName\": \"parentid\",\n            \"displayName\": \"上级菜单\",\n            \"columnType\": \"selector\",\n            \"dataSourcePluginName\": \"org.supercall.plugins.MenuManagerPlugin\",\n            \"dataSourcePlginFunction\": \"getUpperMenuDataSource\"\n        },\n        {\n            \"columnName\": \"isshow\",\n            \"displayName\": \"是否显示\",\n            \"columnType\": \"selector\",\n            \"dataSourcePluginName\": \"org.supercall.plugins.CommonPlugin\",\n            \"dataSourcePlginFunction\": \"showOrHideDataSource\"\n        }\n    ]\n}');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict`
--

DROP TABLE IF EXISTS `sys_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dictkey` varchar(255) DEFAULT NULL COMMENT '字典键',
  `dictvalues` varchar(255) DEFAULT NULL COMMENT '字典值',
  `dictgroup` varchar(255) DEFAULT NULL COMMENT '字典分组',
  `createtime` datetime DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `updator` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dictkey` (`dictkey`,`dictgroup`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='系统字典表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict`
--

LOCK TABLES `sys_dict` WRITE;
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict` VALUES (9,'订阅号','0','wx:public_type',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_log`
--

DROP TABLE IF EXISTS `sys_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operator` varchar(255) DEFAULT NULL COMMENT '操作者',
  `log` longtext,
  `operate_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_log`
--

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;
INSERT INTO `sys_log` VALUES (2,'1','查看列表','2016-05-08 16:30:47'),(3,'1','[删除字典]{\"id\":25,\"dictkey\":\"2\",\"dictvalues\":\"23\",\"dictgroup\":\"2\",\"createtime\":\"May 8, 2016 4:18:56 PM\",\"creator\":\"1\",\"updatetime\":\"May 8, 2016 4:18:59 PM\",\"updator\":\"1\"}','2016-05-08 16:36:11'),(4,'1','[????]{\"name\":\"1\",\"orders\":1,\"icons\":\"1\",\"urls\":\"1\",\"isshow\":0,\"authname\":\"\"}','2016-05-10 23:22:05'),(5,'1','????{\"name\":\"3\",\"icons\":\"\",\"urls\":\"\",\"isshow\":0,\"authname\":\"\",\"createtime\":\"May 11, 2016 9:21:46 PM\",\"creator\":\"1\"}','2016-05-11 21:21:48'),(6,'1','[????]{\"dictkey\":\"2\",\"dictvalues\":\"2\",\"dictgroup\":\"2\",\"createtime\":\"May 11, 2016 9:34:10 PM\",\"creator\":\"1\"}','2016-05-11 21:34:11'),(7,'1','[????]{\"id\":1,\"dictkey\":\"2\",\"dictvalues\":\"2\",\"dictgroup\":\"23\",\"updatetime\":\"May 11, 2016 9:34:15 PM\",\"updator\":\"1\"}','2016-05-11 21:34:15'),(8,'1','[????]{\"id\":1,\"dictkey\":\"2\",\"dictvalues\":\"2\",\"dictgroup\":\"23\",\"createtime\":\"May 11, 2016 9:34:11 PM\",\"creator\":\"1\",\"updatetime\":\"May 11, 2016 9:34:15 PM\",\"updator\":\"1\"}','2016-05-11 21:34:18'),(9,'1','[????]{\"dictkey\":\"3\",\"dictvalues\":\"3\",\"dictgroup\":\"3\",\"createtime\":\"May 11, 2016 9:34:25 PM\",\"creator\":\"1\"}','2016-05-11 21:34:25'),(10,'1','[????]{\"name\":\"22\",\"icons\":\"\",\"urls\":\"22\",\"authname\":\"\",\"createtime\":\"May 13, 2016 8:39:04 PM\",\"creator\":\"1\"}','2016-05-13 20:39:04'),(11,'1','[????]{\"id\":34,\"name\":\"22\",\"icons\":\"\",\"urls\":\"22\",\"isshow\":1,\"authname\":\"\",\"createtime\":\"May 13, 2016 8:39:04 PM\",\"creator\":\"1\"}','2016-05-13 20:39:33'),(12,'1','[????]{\"name\":\"2\",\"icons\":\"\",\"urls\":\"2\",\"isshow\":0,\"authname\":\"2\",\"createtime\":\"May 13, 2016 8:39:47 PM\",\"creator\":\"1\"}','2016-05-13 20:39:48'),(13,'1','[????]{\"name\":\"2222\",\"icons\":\"\",\"urls\":\"2\",\"isshow\":0,\"authname\":\"2\",\"updatetime\":\"May 13, 2016 8:43:06 PM\",\"updator\":\"1\"}','2016-05-13 20:43:07'),(14,'1','[????]{\"name\":\"2222\",\"icons\":\"\",\"urls\":\"2\",\"isshow\":0,\"authname\":\"2\",\"updatetime\":\"May 13, 2016 8:43:41 PM\",\"updator\":\"1\"}','2016-05-13 20:43:41'),(15,'1','[????]{\"id\":35,\"name\":\"22\",\"icons\":\"\",\"urls\":\"2\",\"isshow\":0,\"authname\":\"2\",\"updatetime\":\"May 13, 2016 8:44:22 PM\",\"updator\":\"1\"}','2016-05-13 20:44:22'),(16,'1','[????]{\"id\":35,\"name\":\"2233\",\"icons\":\"\",\"urls\":\"2\",\"isshow\":0,\"authname\":\"2\",\"updatetime\":\"May 13, 2016 8:44:35 PM\",\"updator\":\"1\"}','2016-05-13 20:44:36'),(17,'1','[????]{\"id\":35,\"name\":\"2233\",\"icons\":\"\",\"urls\":\"2\",\"isshow\":0,\"authname\":\"2\",\"createtime\":\"May 13, 2016 8:39:48 PM\",\"creator\":\"1\",\"updatetime\":\"May 13, 2016 8:44:36 PM\",\"updator\":\"1\"}','2016-05-13 20:44:38'),(18,'1','[????]{\"name\":\"4\",\"icons\":\"\",\"urls\":\"4\",\"isshow\":1,\"authname\":\"4\",\"createtime\":\"May 14, 2016 4:11:39 PM\",\"creator\":\"1\"}','2016-05-14 16:11:40'),(19,'1','[????]{\"id\":34,\"name\":\"42\",\"icons\":\"\",\"urls\":\"4\",\"isshow\":0,\"authname\":\"4\",\"updatetime\":\"May 14, 2016 4:11:46 PM\",\"updator\":\"1\"}','2016-05-14 16:11:47'),(20,'1','[????]{\"id\":34,\"name\":\"42\",\"icons\":\"\",\"urls\":\"4\",\"isshow\":0,\"authname\":\"4\",\"createtime\":\"May 14, 2016 4:11:40 PM\",\"creator\":\"1\",\"updatetime\":\"May 14, 2016 4:11:47 PM\",\"updator\":\"1\"}','2016-05-14 16:11:51'),(21,'1','[????]{\"id\":1,\"name\":\"??\",\"orders\":1,\"icons\":\"nav-home\",\"urls\":\"#\",\"isshow\":1,\"authname\":\"\",\"updatetime\":\"May 14, 2016 4:22:20 PM\",\"updator\":\"1\"}','2016-05-14 16:22:21'),(22,'1','[编辑菜单]{\"id\":1,\"name\":\"首页\",\"orders\":1,\"icons\":\"nav-home\",\"urls\":\"#\",\"isshow\":1,\"authname\":\"\",\"updatetime\":\"May 14, 2016 4:33:07 PM\",\"updator\":\"1\"}','2016-05-14 16:33:08'),(23,'1','[新增角色]{\"name\":\"4,3\",\"comment\":\"4,3\",\"createtime\":\"May 14, 2016 5:11:43 PM\",\"creator\":\"1\"}','2016-05-14 17:11:44'),(24,'1','[删除角色]{\"id\":4,\"name\":\"4,3\",\"comment\":\"4,3\",\"createtime\":\"May 14, 2016 5:11:43 PM\",\"creator\":\"1\"}','2016-05-14 17:11:51'),(25,'1','[新增角色]{\"name\":\"4\",\"comment\":\"4\",\"createtime\":\"May 14, 2016 5:14:31 PM\",\"creator\":\"1\"}','2016-05-14 17:14:31'),(26,'1','[编辑角色]{\"id\":5,\"name\":\"43\",\"comment\":\"4\",\"updatetime\":\"May 14, 2016 5:16:17 PM\",\"updator\":\"1\"}','2016-05-14 17:16:18'),(27,'1','[新增用户]{\"name\":\"333\",\"comment\":\"\",\"enabled\":1,\"password\":\"c4ca4238a0b923820dcc509a6f75849b\",\"accounts\":\"1\",\"createtime\":\"May 15, 2016 3:49:29 PM\",\"creator\":\"1\"}','2016-05-15 15:49:29'),(28,'1','[编辑用户]{\"id\":4,\"name\":\"3333\",\"comment\":\"\",\"enabled\":1,\"password\":\"c4ca4238a0b923820dcc509a6f75849b\",\"accounts\":\"1\",\"updatetime\":\"May 15, 2016 3:49:47 PM\",\"updator\":\"1\"}','2016-05-15 15:49:47'),(29,'1','[????]{\"dictkey\":\"?\",\"dictvalues\":\"1\",\"dictgroup\":\"sys_user:sex\",\"createtime\":\"Jul 30, 2016 6:41:53 PM\",\"creator\":\"1\"}','2016-07-30 18:41:53'),(30,'1','[????]{\"dictkey\":\"?\",\"dictvalues\":\"2\",\"dictgroup\":\"sys_user:sex\",\"createtime\":\"Jul 30, 2016 6:41:59 PM\",\"creator\":\"1\"}','2016-07-30 18:41:59'),(31,'1','[删除字典]{\"id\":3,\"dictkey\":\"?\",\"dictvalues\":\"1\",\"dictgroup\":\"sys_user:sex\",\"createtime\":\"Jul 30, 2016 6:41:53 PM\",\"creator\":\"1\"}','2016-07-30 18:43:31'),(32,'1','[新增字典]{\"dictkey\":\"男\",\"dictvalues\":\"1\",\"dictgroup\":\"1\",\"createtime\":\"Jul 30, 2016 6:43:39 PM\",\"creator\":\"1\"}','2016-07-30 18:43:39'),(33,'1','[删除字典]{\"id\":5,\"dictkey\":\"男\",\"dictvalues\":\"1\",\"dictgroup\":\"1\",\"createtime\":\"Jul 30, 2016 6:43:39 PM\",\"creator\":\"1\"}','2016-07-30 18:43:44'),(34,'1','[新增字段]{\"dictkey\":\"1\",\"dictvalues\":\"1\",\"dictgroup\":\"1\",\"createtime\":\"Jul 30, 2016 9:43:39 PM\",\"creator\":\"1\"}','2016-07-30 21:43:39'),(35,'1','[新增字段]{\"dictkey\":\"2\",\"dictvalues\":\"2\",\"dictgroup\":\"1\",\"createtime\":\"Jul 30, 2016 9:43:44 PM\",\"creator\":\"1\"}','2016-07-30 21:43:44'),(36,'1','[编辑字段]{\"id\":10,\"dictkey\":\"2\",\"dictvalues\":\"2\",\"dictgroup\":\"13\",\"updatetime\":\"Jul 30, 2016 9:43:49 PM\",\"updator\":\"1\"}','2016-07-30 21:43:49'),(37,'1','[删除字段]{\"id\":10,\"dictkey\":\"2\",\"dictvalues\":\"2\",\"dictgroup\":\"13\",\"createtime\":\"Jul 30, 2016 9:43:44 PM\",\"creator\":\"1\",\"updatetime\":\"Jul 30, 2016 9:43:49 PM\",\"updator\":\"1\"}','2016-07-30 21:43:51'),(38,'1','[删除字段]{\"id\":9,\"dictkey\":\"1\",\"dictvalues\":\"1\",\"dictgroup\":\"1\",\"createtime\":\"Jul 30, 2016 9:43:39 PM\",\"creator\":\"1\"}','2016-07-30 21:43:52');
/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `orders` int(11) DEFAULT NULL COMMENT '菜单排序',
  `icons` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `urls` varchar(255) DEFAULT NULL COMMENT '菜单地址',
  `parentid` int(11) DEFAULT NULL COMMENT '上级菜单ID',
  `isshow` int(11) DEFAULT NULL COMMENT '是否显示 0 是 1 否',
  `authname` varchar(255) DEFAULT NULL COMMENT '权限注解',
  `createtime` datetime DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `updator` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `authname` (`authname`),
  CONSTRAINT `sys_menu_ibfk_1` FOREIGN KEY (`parentid`) REFERENCES `sys_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,'首页',1,'icon fa fa-desktop','#',NULL,1,'',NULL,NULL,'2016-05-14 16:33:08','1'),(2,'系统管理',2,'icon fa fa-cubes','#',NULL,1,NULL,NULL,NULL,NULL,NULL),(3,'角色管理',21,' ','/admin/roles/index',2,1,NULL,NULL,NULL,NULL,NULL),(4,'菜单管理',1,' ','/framework/simplegrid/menu',2,1,NULL,NULL,NULL,NULL,NULL),(6,'用户管理',3,' ','/framework/simplegrid/menu',2,1,NULL,NULL,NULL,NULL,NULL),(7,'仪表盘',4,' ','/admin/index',1,1,'ROLE_SYS_MAINFORM',NULL,NULL,NULL,NULL),(8,'字典管理',5,'nav-home','/framework/simplegrid/dict',2,1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `comment` varchar(255) DEFAULT NULL COMMENT '角色备注',
  `createtime` datetime DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `updator` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='系统角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'系统管理员','系统管理员',NULL,NULL,NULL,NULL),(3,'3','3',NULL,NULL,NULL,NULL),(5,'43','4','2016-05-14 17:14:31','1','2016-05-14 17:16:18','1');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_auth`
--

DROP TABLE IF EXISTS `sys_role_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  `authname` varchar(255) DEFAULT NULL COMMENT '角色权限',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`),
  KEY `sys_role_auth_ibfk_1` (`authname`),
  KEY `roleid` (`roleid`),
  CONSTRAINT `sys_role_auth_ibfk_1` FOREIGN KEY (`authname`) REFERENCES `sys_menu` (`authname`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_auth_ibfk_2` FOREIGN KEY (`roleid`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COMMENT='角色权限记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_auth`
--

LOCK TABLES `sys_role_auth` WRITE;
/*!40000 ALTER TABLE `sys_role_auth` DISABLE KEYS */;
INSERT INTO `sys_role_auth` VALUES (28,3,NULL,NULL),(34,3,NULL,NULL),(39,3,NULL,NULL),(45,3,NULL,NULL);
/*!40000 ALTER TABLE `sys_role_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menuid` (`menuid`),
  KEY `roleid` (`roleid`),
  CONSTRAINT `sys_role_menu_ibfk_1` FOREIGN KEY (`menuid`) REFERENCES `sys_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_menu_ibfk_2` FOREIGN KEY (`roleid`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='角色菜单关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES (4,3,1),(5,4,1),(6,6,1),(9,7,3),(11,3,3),(17,4,3),(22,6,3),(28,8,3),(33,7,5);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_user`
--

DROP TABLE IF EXISTS `sys_role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roleid` (`roleid`),
  KEY `userid` (`userid`),
  CONSTRAINT `sys_role_user_ibfk_2` FOREIGN KEY (`roleid`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_user_ibfk_3` FOREIGN KEY (`userid`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='角色用户关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_user`
--

LOCK TABLES `sys_role_user` WRITE;
/*!40000 ALTER TABLE `sys_role_user` DISABLE KEYS */;
INSERT INTO `sys_role_user` VALUES (3,1,1),(8,1,5);
/*!40000 ALTER TABLE `sys_role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `accounts` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `updator` varchar(255) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts` (`accounts`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,'1','1',1,'c4ca4238a0b923820dcc509a6f75849b','1',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_publicservice`
--

DROP TABLE IF EXISTS `wx_publicservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_publicservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '公众号名称',
  `typeid` int(11) DEFAULT NULL COMMENT '公众号类型',
  `rawid` varchar(255) DEFAULT NULL COMMENT '原始ID',
  `appid` varchar(255) DEFAULT NULL,
  `appsec` varchar(45) DEFAULT NULL COMMENT '应用密钥',
  `token` varchar(45) DEFAULT NULL COMMENT '微信令牌',
  `aeskey` varchar(45) DEFAULT NULL,
  `desctype` int(11) DEFAULT NULL COMMENT '加密方式',
  PRIMARY KEY (`id`),
  CONSTRAINT `desctype` FOREIGN KEY (`id`) REFERENCES `sys_dict` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `typeid` FOREIGN KEY (`id`) REFERENCES `sys_dict` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信公众号';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_publicservice`
--

LOCK TABLES `wx_publicservice` WRITE;
/*!40000 ALTER TABLE `wx_publicservice` DISABLE KEYS */;
/*!40000 ALTER TABLE `wx_publicservice` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-03 23:24:19
