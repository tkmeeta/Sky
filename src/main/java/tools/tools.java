package tools;

import com.google.gson.Gson;
import org.supercall.domainModel.framework.FormConfig;
import org.supercall.domainModel.framework.GridColumnConfig;
import org.supercall.domainModel.framework.SimpleGridConfigModel;
import org.supercall.domainModel.framework.TableFilterConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kira on 16/8/3.
 */
public class tools {
    public static void main(String[] args) {
        dict();
    }

    static List<GridColumnConfig> buildColumnConfig(String str) {
        List<GridColumnConfig> columnConfig = new ArrayList<>();
        String[] config = str.split(",");
        for (String s : config) {
            columnConfig.add(new GridColumnConfig(s));
        }
        return columnConfig;
    }

    static List<TableFilterConfig> buildTableFilter(String str) {
        List<TableFilterConfig> tableFilterConfig = new ArrayList<>();
        String[] config = str.split(",");
        for (String s : config) {
            String[] item = s.split("_");
            tableFilterConfig.add(new TableFilterConfig(item[0], item[1], item[2], item[3] == "" ? null : item[3], item[4] == "" ? null : item[4]));
        }
        return tableFilterConfig;
    }

    static void dict() {
        List<GridColumnConfig> columnConfig = buildColumnConfig("id,字段分组,字典键,字典值");

        List<TableFilterConfig> tableFilterConfig = buildTableFilter(
                "dictgroup_字典分组_input_ _ ," +
                        "dictkey_字段键_input_ _ "
        );

        List<FormConfig> formConfigs = buildformConfigs(
                "字典分组_dictgroup_input_ _ _true_ ," +
                        "字典键_dictkey_input_ _ _true_ ," +
                        "字典值_dictvalues_input_ _ _true_ "
        );
        SimpleGridConfigModel simpleGridConfigModel =
                new SimpleGridConfigModel("org.supercall.dao.SysDictMapper", columnConfig, tableFilterConfig, formConfigs);
        System.out.println(new Gson().toJson(simpleGridConfigModel));
    }

    static List<FormConfig> buildformConfigs(String str) {
        List<FormConfig> formConfigs = new ArrayList<>();
        String[] configs = str.split(",");
        for (String config : configs) {
            String[] item = config.split("_");
            formConfigs.add(new FormConfig(item[0],
                    item[1],
                    item[2], item[3],
                    item[4],
                    Boolean.valueOf(item[5]),
                    item[6]));
        }
        return formConfigs;
    }

    static void menu() {
        List<GridColumnConfig> columnConfig = buildColumnConfig("id," +
                "上级菜单,菜单名称,菜单排序,菜单图标,菜单地址,是否隐藏");

        List<TableFilterConfig> tableFilterConfig = buildTableFilter(
                "name_菜单名称_input_ _," +
                        "parentid_上级菜单_selector_org.supercall.plugins.MenuManagerPlugin_getUpperMenuDataSource," +
                        "isshow_是否显示_selector_org.supercall.plugins.CommonPlugin_showOrHideDataSource"
        );

        List<FormConfig> formConfigs = new ArrayList<>();
        SimpleGridConfigModel simpleGridConfigModel = new SimpleGridConfigModel("org.supercall.dao.SysMenuMapper",
                columnConfig, tableFilterConfig, formConfigs);
        System.out.println(new Gson().toJson(simpleGridConfigModel));
    }
}
