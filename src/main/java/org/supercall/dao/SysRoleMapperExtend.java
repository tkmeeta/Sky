package org.supercall.dao;

import org.springframework.stereotype.Repository;
import org.supercall.common.IListPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface SysRoleMapperExtend extends IListPlugin {
    List<HashMap<String, Object>> loadRoleViaUser(Map<String, String> map);
}