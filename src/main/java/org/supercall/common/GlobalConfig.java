package org.supercall.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 全局配置器
 */
@Component
public class GlobalConfig {

    @Value("${env.isDev}")
    private Boolean isDev;

    public Boolean getDev() {
        return isDev;
    }

    public void setDev(Boolean dev) {
        isDev = dev;
    }
}
