package org.supercall.common;

import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.supercall.domainModel.FilterLists;
import org.supercall.domainModel.TableHeader;
import org.supercall.domainModel.framework.SimpleGridConfigModel;
import org.supercall.mybatis.Pagination;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by kira on 16/7/26.
 */
public class CommonUtility {

    //Spring上下文容器
    public static ApplicationContext applicationContext;


    /**
     * Request转Map
     *
     * @param request
     * @return
     */
    public static HashMap<String, Object> getParameterMap(
            HttpServletRequest request) {
        // 参数Map
        Map properties = request.getParameterMap();
        // 返回值Map
        Map returnMap = new HashMap();
        Iterator entries = properties.entrySet().iterator();
        Map.Entry entry;
        String name = "";
        String value = "";
        while (entries.hasNext()) {
            entry = (Map.Entry) entries.next();
            name = (String) entry.getKey();
            Object valueObj = entry.getValue();
            if (null == valueObj) {
                value = "";
            } else if (valueObj instanceof String[]) {
                String[] values = (String[]) valueObj;
                for (int i = 0; i < values.length; i++) {
                    value = values[i] + ",";
                }
                value = value.substring(0, value.length() - 1);
            } else {
                value = valueObj.toString();
            }
            returnMap.put(name, value);
        }
        return (HashMap<String, Object>) returnMap;
    }


    /**
     * 简单列表
     *
     * @param iListPlugin
     * @param request
     * @param simpleGridConfigModel
     * @return
     */
    public static HashMap<String, Object> buildSimpleList(IListPlugin iListPlugin,
                                                          HttpServletRequest request,
                                                          SimpleGridConfigModel simpleGridConfigModel) {
        try {
            //构造分页器
            String offset = request.getParameter("offset");
            String limit = request.getParameter("limit");
            int off = StringUtils.isEmpty(offset) ? 0 : Integer.parseInt(offset);
            int lim = StringUtils.isEmpty(limit) ? 10 : Integer.parseInt(limit);
            Pagination pagination = new Pagination(off, lim);

            //构造表格数据
            HashMap<String, Object> map = CommonUtility.getParameterMap(request);
            List<LinkedHashMap<String, Object>> result = iListPlugin.all(pagination, map);
            HashMap<String, Object> tableEntity = new HashMap<>();
            tableEntity.put("tableContent", result);

            //设定分页相关信息
            int total = 0;
            if (pagination.getTotal() <= lim) {
                total = 1;
            }

            if (pagination.getTotal() > lim) {
                int num = pagination.getTotal() / lim;
                if (num == 1) {
                    total = 2;
                } else {
                    total = pagination.getTotal() / Integer.valueOf(limit);
                }
            }

            tableEntity.put("total", total);
            tableEntity.put("limit", lim);
            tableEntity.put("offset", off);
            tableEntity.put("currentpage", (off / lim) + 1);

            //构造列表过滤器
            List<FilterLists> filterLists = new ArrayList<>();
            simpleGridConfigModel.getTableFilterConfigs().forEach(e -> {
                Object datasources = null;

                //下拉列表,需要加载指定数据源
                if (e.getColumnType().equals("selector")) {
                    try {
                        Class classType = Class.forName(e.getDataSourcePluginName());
                        Object invoker = classType.getConstructor(new Class[]{}).newInstance(new Object[]{});
                        Method method = classType.getMethod(e.getDataSourcePlginFunction());
                        datasources = method.invoke(invoker);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                FilterLists filterEntity = new FilterLists(e.getDisplayName(), e.getColumnName(), e.getColumnType(), "", datasources);

                if (map.get(filterEntity.getKey()) != null) {
                    filterEntity.setValue((String) map.get(filterEntity.getKey()));
                }
                filterLists.add(filterEntity);

            });
            tableEntity.put("filterLists", filterLists);


            //构造表头
            List<TableHeader> tableHeaders = new ArrayList<>();
            simpleGridConfigModel.getGridColumnConfigs().forEach(e -> {
                tableHeaders.add(new TableHeader(e.getColunmName(), "", ""));
                tableEntity.put("tableHeader", tableHeaders);
            });
            return tableEntity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
