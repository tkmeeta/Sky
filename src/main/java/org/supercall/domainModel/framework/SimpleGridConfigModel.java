package org.supercall.domainModel.framework;

import java.util.List;

/**
 * Created by kira on 16/8/3.
 */
public class SimpleGridConfigModel {
    private String entityName;

    private List<GridColumnConfig> gridColumnConfigs;

    private List<TableFilterConfig> tableFilterConfigs;

    private List<FormConfig> formConfigs;

    public SimpleGridConfigModel(String entityName, List<GridColumnConfig> gridColumnConfigs, List<TableFilterConfig> tableFilterConfigs, List<FormConfig> formConfigs) {
        this.entityName = entityName;
        this.gridColumnConfigs = gridColumnConfigs;
        this.tableFilterConfigs = tableFilterConfigs;
        this.formConfigs = formConfigs;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public List<GridColumnConfig> getGridColumnConfigs() {
        return gridColumnConfigs;
    }

    public void setGridColumnConfigs(List<GridColumnConfig> gridColumnConfigs) {
        this.gridColumnConfigs = gridColumnConfigs;
    }

    public List<TableFilterConfig> getTableFilterConfigs() {
        return tableFilterConfigs;
    }

    public void setTableFilterConfigs(List<TableFilterConfig> tableFilterConfigs) {
        this.tableFilterConfigs = tableFilterConfigs;
    }

    public List<FormConfig> getFormConfigs() {
        return formConfigs;
    }

    public void setFormConfigs(List<FormConfig> formConfigs) {
        this.formConfigs = formConfigs;
    }
}
