package org.supercall.models;

public class SysConfig {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_config.id
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_config.uid
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    private String uid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_config.metaconfig
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    private String metaconfig;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_config.id
     *
     * @return the value of sys_config.id
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_config.id
     *
     * @param id the value for sys_config.id
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_config.uid
     *
     * @return the value of sys_config.uid
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    public String getUid() {
        return uid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_config.uid
     *
     * @param uid the value for sys_config.uid
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_config.metaconfig
     *
     * @return the value of sys_config.metaconfig
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    public String getMetaconfig() {
        return metaconfig;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_config.metaconfig
     *
     * @param metaconfig the value for sys_config.metaconfig
     *
     * @mbggenerated Wed Aug 03 22:43:27 CST 2016
     */
    public void setMetaconfig(String metaconfig) {
        this.metaconfig = metaconfig;
    }
}