    {
        "entityName": "org.supercall.dao.SysDictMapper",
        "add_form_name": "新增字典",
        "edit_form_name": "编辑字典",
        "addPlugins": "",
        "deletePlugins": "",
        "editPlugins": "",
        "gridColumnConfigs": [
            {
                "colunmName": "id"
            }
        ],
        "tableFilterConfigs": [
            {
                "columnName": "dictgroup",
                "displayName": "字典分组",
                "columnType": "input",
                "dataSourcePluginName": " ",
                "dataSourcePlginFunction": " "
            }
        ],
        "formConfigs": [
            {
                "displayName": "字典分组",
                "key": "dictgroup",
                "inputType": "input",
                "pluginName": " ",
                "pluginFunction": " ",
                "isRequire": true,
                "validateType": " ",
                "value": ""
            }
        ]
    }